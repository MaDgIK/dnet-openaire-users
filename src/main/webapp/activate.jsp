<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en-gb" dir="ltr" vocab="http://schema.org/">
<jsp:include page="head.jsp">
  <jsp:param name="title" value="OpenAIRE - Activate Account"/>
  <jsp:param name="form" value="true"/>
</jsp:include>
<body>
  <div class="uk-section uk-section-small uk-container uk-container-small">
    <div class="uk-text-center">
      <img src="images/Logo_Horizontal.png" style="height: 80px;">
      <h1 class="uk-h4 uk-margin-large-top">Thank you for registering!</h1>
      <div class="uk-text-large uk-margin-medium-bottom">
        The next step is to <span class="uk-text-bolder">activate your account.</span>
      </div>
      <div class="uk-margin-large-bottom">
        An <span class="uk-text-bolder">email</span> with your username and an <span
          class="uk-text-bolder">activation code</span>
        has been sent to you. Please use them in the form below to activate your account.
        The activation code <span class="uk-text-bold uk-text-warning">expires in 24 hours</span>.
      </div>
    </div>
    <form action="activate" method="POST" role="form"
          class="uk-grid uk-child-width-1-1 uk-flex-column uk-flex-middle" uk-grid>
      <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
      <div input id="username" class="uk-width-medium@s">
        <div class="input-wrapper inner x-small">
          <div class="input-box">
            <div class="placeholder">
              <label>Username <sup>*</sup></label>
            </div>
            <div class="uk-flex uk-flex-middle">
              <input name="username" class="input uk-text-truncate">
            </div>
          </div>
        </div>
        <span id="server_username_error" class="uk-text-danger uk-text-small">
          ${msg_username_error}
        </span>
        <c:remove var="msg_username_error" scope="session" />
      </div>
      <div input id="verification_code" class="uk-width-medium@s">
        <div class="input-wrapper inner x-small">
          <div class="input-box">
            <div class="placeholder">
              <label>Activation Code <sup>*</sup></label>
            </div>
            <div class="uk-flex uk-flex-middle">
              <input name="verification_code" value="${param.code}" class="input uk-text-truncate">
            </div>
          </div>
        </div>
        <span id="server_activation_code_error" class="uk-text-danger uk-text-small">
          ${msg_activation_code_error}
        </span>
        <c:remove var="msg_activation_code_error" scope="session" />
      </div>
      <div class="uk-width-1-1 uk-text-center">
        <div id="server_error" class="uk-text-danger uk-text-center uk-text-small uk-margin-bottom">${message}</div>
        <c:remove var="message" scope="session" />
        <button type="submit" class="uk-button uk-button-primary" onclick="return validateForm();">
          Activate
        </button>
      </div>
    </form>
  </div>
</body>
<script>
    $("#username input").focusin(function() {
        $("#server_username_error").hide();
        $("#server_error").hide();
    });

    $("#verification_code input").focusin(function() {
        $("#server_activation_code_error").hide();
        $("#server_error").hide();
    });
</script>
</html>
