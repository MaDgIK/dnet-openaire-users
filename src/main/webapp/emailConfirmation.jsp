<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<jsp:include page="head.jsp">
  <jsp:param name="title" value="OpenAIRE - Email Confirmation"/>
</jsp:include>
<body>
  <div class="uk-section uk-section-small uk-container uk-container-small uk-text-center">
    <img src="images/Logo_Horizontal.png" style="height: 80px;">
    <h1 class="uk-h4 uk-margin-large-top">PLEASE VERIFY YOUR EMAIL</h1>
    <div class="uk-text-large">You will need to verify your email to complete registration.</div>
    <img class="uk-margin-large-top uk-margin-large-bottom" src="images/envelope.svg" style="height: 140px;">
    <div class="uk-margin-medium-bottom">
      An email has been sent to your email with a link to verify your account.<br>
      If you have not received the email after a few minutes, please check your spam folder.
    </div>
    <a class="uk-button uk-button-secondary" href="mailto:aai@openaire.eu">Contact Support</a>
  </div>
</body>
</html>
