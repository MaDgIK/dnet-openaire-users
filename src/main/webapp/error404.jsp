<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en-gb" dir="ltr" vocab="http://schema.org/">
<jsp:include page="head.jsp">
    <jsp:param name="title" value="OpenAIRE - Error 404"/>
</jsp:include>
<body>
<div class="uk-section uk-section-small uk-container uk-container-small">
    <div class="uk-text-center">
        <img src="images/Logo_Horizontal.png" style="height: 80px;">
        <h1 class="uk-h4 uk-text-danger uk-margin-large-top">Oops! Something went wrong!</h1>
        <div class="uk-margin-large-bottom uk-margin-medium-top">
            404 Error! The requested page was not found.
        </div>
    </div>
</div>
</body>
</html>
