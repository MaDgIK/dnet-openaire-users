<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="tm-header  tm-header-transparent" uk-header="">
    <div class="uk-container uk-container-expand">
        <nav class="uk-navbar" uk-navbar="{&quot;align&quot;:&quot;left&quot;}">
            <div class="uk-navbar-center">
                <div class="uk-logo uk-navbar-item">
                    <img alt="OpenAIRE" class="uk-responsive-height" src="./images/Logo_Horizontal.png">
                </div>
            </div>
            <!-- user menu -->
            <div class=uk-navbar-right>
                <ul class="uk-navbar-nav user_actions">
                    <c:choose>
                    <c:when test="${not authenticated}">
                        <li><a href="./openid_connect_login"> Sign in </a></li>
                    </c:when>
                    <c:otherwise>
                    <li>
                        <a class="login" aria-expanded="false">
                            <svg height="60" width="60">
                                <circle cx="30" cy="30" r="20" stroke-width="2"></circle>
                                <text dy=".4em" font-size="16" text-anchor="middle" x="50%" y="50%"
                                      class="ng-star-inserted">
                                        ${name}
                                </text>
                            </svg>
                        </a>
                        </c:otherwise>
                        </c:choose>
                        <div class="uk-navbar-dropdown uk-navbar-dropdown-bottom-right" id="userMenu"
                             style="left: 344.433px; top: 100px;">
                            <div class="uk-navbar-dropdown-grid uk-child-width-1-1 uk-grid uk-grid-stack"
                                 uk-grid="">
                                <div class="uk-first-column uk-height-max-medium uk-overflow-auto">
                                    <ul class="uk-nav uk-navbar-dropdown-nav">
                                        <li class=""><a href="./personalToken">Personal token</a></li>
                                        <li class=""><a href="./registeredServices">Registered services</a></li>
                                        <li class="uk-nav-divider "></li>
                                        <li>
                                            <a href="./openid_logout" id="logout">Log out</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <!-- USER MENU ENDS HERE -->
        </nav>
    </div>
</div>
