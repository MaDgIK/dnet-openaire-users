<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="uk-section-primary uk-section uk-section-small">
    <div class="uk-container">
        <div class="uk-grid-margin uk-grid uk-grid-stack" uk-grid="">
            <div class="uk-width-1-1@m uk-first-column">
                <div class="uk-margin uk-margin-remove-top uk-margin-remove-bottom uk-text-center">
                    <img alt="OpenAIRE" class="el-image" src="./images/Logo_Horizontal_white_small.png">
                </div>
                <div class="footer-license uk-margin uk-margin-remove-bottom uk-text-center uk-text-lead">
                    <div><a href="http://creativecommons.org/licenses/by/4.0/" target="_blank"
                            rel="license"><img alt="Creative" src="./images/80x15.png"
                                               style="height: auto; max-width: 100%; vertical-align: middle;"></a>&nbsp;UNLESS
                        OTHERWISE INDICATED, ALL MATERIALS CREATED BY THE OPENAIRE CONSORTIUM ARE LICENSED UNDER
                        A&nbsp;<a href="http://creativecommons.org/licenses/by/4.0/" rel="license">CREATIVE
                            COMMONS ATTRIBUTION 4.0 INTERNATIONAL LICENSE</a>.
                    </div>
                    <div>OPENAIRE IS POWERED BY&nbsp;<a href="http://www.d-net.research-infrastructures.eu/">D-NET</a>.
                    </div>
                </div>
                <div class="uk-margin uk-margin-remove-top uk-margin-remove-bottom uk-text-right">
                    <a class="uk-totop uk-icon" href="#" uk-scroll="" uk-totop="">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
