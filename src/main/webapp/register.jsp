<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en-gb" dir="ltr" vocab="http://schema.org/">
<jsp:include page="head.jsp">
    <jsp:param name="title" value="OpenAIRE - Register"/>
    <jsp:param name="form" value="true"/>
</jsp:include>
<body>
<div class="uk-section uk-section-small uk-container uk-container-small uk-flex uk-flex-column uk-flex-middle">
    <div class="uk-text-center uk-margin-large-bottom">
        <img src="images/Logo_Horizontal.png" style="height: 80px;">
        <h1 class="uk-h4 uk-margin-large-top">Create a new OpenAIRE account</h1>
    </div>
    <form action="register" method="POST" role="form"
          class="uk-grid uk-width-xlarge uk-child-width-1-2@m uk-child-width-1-1 uk-flex-top" uk-grid>
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        <div input id="first_name">
            <div class="input-wrapper inner x-small">
                <div class="input-box">
                    <div class="placeholder">
                        <label>First Name <sup>*</sup></label>
                    </div>
                    <div class="uk-flex uk-flex-middle">
                        <input name="first_name" class="input uk-text-truncate" value=${first_name}>
                    </div>
                </div>
            </div>
            <div id="msg_first_name_error" class="uk-text-danger uk-text-small" style="display: none;">Please enter your first name.</div>
            <c:remove var="msg_first_name_error_display" scope="session" />
            <c:remove var="first_name" scope="session" />
        </div>
        <div input id="last_name">
            <div class="input-wrapper inner x-small">
                <div class="input-box">
                    <div class="placeholder">
                        <label>Last Name <sup>*</sup></label>
                    </div>
                    <div class="uk-flex uk-flex-middle">
                        <input name="last_name" class="input uk-text-truncate" value=${last_name}>
                    </div>
                </div>
            </div>
            <div id="msg_last_name_error" class="uk-text-danger uk-text-small" style="display: none;">Please enter your last name.</div>
            <c:remove var="msg_last_name_error_display" scope="session" />
            <c:remove var="last_name" scope="session" />
        </div>
        <div input id="organization">
            <div class="input-wrapper inner x-small">
                <div class="input-box">
                    <div class="placeholder">
                        <label>Affiliation / Organization</label>
                    </div>
                    <div class="uk-flex uk-flex-middle">
                        <input name="organization" class="input uk-text-truncate" value=${organization}>
                    </div>
                </div>
                <c:remove var="organization" scope="session" />
            </div>
        </div>
        <div input id="username">
            <div class="input-wrapper inner x-small">
                <div class="input-box">
                    <div class="placeholder">
                        <label>Username <sup>*</sup></label>
                    </div>
                    <div class="uk-flex uk-flex-middle">
                        <input name="username" class="input uk-text-truncate" value=${username}>
                    </div>
                </div>
            </div>
            <div id="msg_username_error" class="uk-text-danger uk-text-small" style="display: none;">Please enter your username.</div>
            <div id="msg_username_min_length" class="uk-text-danger uk-text-small" style="display: none;">Minimum username length 5 characters.</div>
            <div id="msg_username_max_length" class="uk-text-danger uk-text-small" style="display: none;">Maximum username length 150 characters.</div>
            <div id="msg_username_start" class="uk-text-danger uk-text-small" style="display: none;">Username must start with a letter or digit.</div>
            <div id="msg_username_allowed_characters" class="uk-text-danger uk-text-small" style="display: none;">You can use letters, numbers, underscores, hyphens and periods.</div>
            <div id="username_server_error" class="uk-text-danger uk-text-small">${username_message}</div>
            <div id="username_allowed_chars_server_error" class="uk-text-danger uk-text-small">${username_allowed_chars_message}</div>
            <div id="username_first_char_server_error" class="uk-text-danger uk-text-small">${username_first_char_message}</div>
            <c:remove var="username" scope="session" />
            <c:remove var="username_message" scope="session" />
            <c:remove var="username_allowed_chars_message" scope="session" />
            <c:remove var="username_first_char_message" scope="session" />
        </div>
        <div input id="email">
            <div class="input-wrapper inner x-small">
                <div class="input-box">
                    <div class="placeholder">
                        <label>Email <sup>*</sup></label>
                    </div>
                    <div class="uk-flex uk-flex-middle">
                        <input name="email" class="input uk-text-truncate" value=${email}>
                    </div>
                </div>
            </div>
            <div id="msg_email_error" class="uk-text-danger uk-text-small" style="display: none;">Please enter your email.</div>
            <div id="msg_email_validation_error" class="uk-text-danger uk-text-small" style="display: none;">Please enter a valid email.</div>
            <div id="msg_email_conf_error" class="uk-text-danger uk-text-small" style="display: none;">The emails don't match.</div>
            <div id="email_server_error" class="uk-text-danger uk-text-small">${email_message}</div>
            <c:remove var="email" scope="session" />
            <c:remove var="msg_email_error_display" scope="session" />
            <c:remove var="msg_email_conf_error_display" scope="session" />
            <c:remove var="msg_email_validation_error_display" scope="session" />
            <c:remove var="email_message" scope="session" />
        </div>
        <div input id="email_conf">
            <div class="input-wrapper inner x-small">
                <div class="input-box">
                    <div class="placeholder">
                        <label>Confirm Email <sup>*</sup></label>
                    </div>
                    <div class="uk-flex uk-flex-middle">
                        <input name="email_conf" class="input uk-text-truncate" value=${email_conf}>
                    </div>
                </div>
            </div>
            <c:remove var="email_conf" scope="session" />
        </div>
        <div input id="password">
            <div class="input-wrapper inner x-small">
                <div class="input-box">
                    <div class="placeholder">
                        <label>Password <sup>*</sup></label>
                    </div>
                    <div class="uk-flex uk-flex-middle">
                        <input name="password" type="password" class="input uk-text-truncate">
                    </div>
                </div>
            </div>
            <div id="msg_pass_error" class="uk-text-danger uk-text-small" style="display: none;">Please enter your password.</div>
            <div id="msg_pass_conf_error" class="uk-text-danger uk-text-small" style="display: none;">The passwords don't match.</div>
            <div id="msg_whitespace" class="uk-text-danger uk-text-small" style="display: none;">No white space allowed.</div>
            <div id="msg_lowercase_letter" class="uk-text-danger uk-text-small" style="display: none;">Please add a lowercase letter.</div>
            <div id="msg_uppercase_letter" class="uk-text-danger uk-text-small" style="display: none;">Please add an uppercase letter.</div>
            <div id="msg_number" class="uk-text-danger uk-text-small" style="display: none;">Please add a number.</div>
            <div id="msg_length" class="uk-text-danger uk-text-small" style="display: none;">Must contains at least 6 characters.</div>
            <div id="msg_invalid_password" class="uk-text-danger uk-text-small" style="display: none;">
                The password must contain a lowercase letter, a capital (uppercase) letter, a number and must be at least 6 characters long. White space character is not allowed.
            </div>
            <c:remove var="msg_pass_conf_error_display" scope="session" />
            <c:remove var="msg_password_error_display" scope="session" />
            <c:remove var="msg_invalid_password_display" scope="session" />
        </div>
        <div input id="password_conf">
            <div class="input-wrapper inner x-small">
                <div class="input-box">
                    <div class="placeholder">
                        <label>Confirm Password <sup>*</sup></label>
                    </div>
                    <div class="uk-flex uk-flex-middle">
                        <input name="password_conf" type="password" class="input uk-text-truncate">
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-width-1-1 uk-flex uk-flex-center">
            <div class="uk-text-center">
                <span id="recaptcha_error" class="uk-text-danger uk-text-small" style="display:none;">You missed the reCAPTCHA validation!</span>
                <div class="g-recaptcha" data-sitekey=${applicationScope.sitekey}></div>
            </div>
            <c:remove var="recaptcha_error_display" scope="session" />
        </div>
        <div class="uk-width-1-1 uk-text-center">
            <div id="server_error" class="uk-text-danger uk-text-center uk-text-small uk-margin-bottom">${message}</div>
            <c:remove var="message" scope="session" />
            <button type="submit" class="uk-button uk-button-primary" onclick="return validateForm();">
                Register
            </button>
        </div>
    </form>
</div>
</body>
<script>
    $("input").focusin(function () {
        $("#server_error").hide();
    });

    // On the fly check for username validation
    let username = $("#username input")[0];
    username.onkeyup = function () {
        if (username.value.length < 5) {
            $("#msg_username_min_length").show();
        } else {
            $("#msg_username_min_length").hide();
            if (username.value.length >= 150) {
                $("#msg_username_min_length").show();
            } else {
                $("#msg_username_min_length").hide();
                let allowedChars = /^[a-zA-Z0-9._-]*$/;
                if (!username.value.match(allowedChars)) {
                    $("#msg_username_allowed_characters").show();
                } else {
                    $("#msg_username_allowed_characters").hide();
                    let startsWith = /^[a-zA-Z0-9].*/;
                    if (!username.value.match(startsWith)) {
                        $("#msg_username_start").show();
                    } else {
                        $("#msg_username_start").hide();
                    }
                }
            }
        }
    };

    // On the fly check for password validation
    let password = $("#password input")[0];
    password.onkeyup = function () {
        validatePassword(password.value);
    };

    $("#first_name input").focusin(function () {
        $("#msg_first_name_error").hide();
    });

    $("#last_name input").focusin(function () {
        $("#msg_last_name_error").hide();
    });

    $("#username input").focusin(function () {
        $("#msg_username_error").hide();
        $("#username_server_error").hide();
        $("#username_allowed_chars_server_error").hide();
        $("#username_first_char_server_error").hide();
    });

    $("#email input").focusin(function () {
        $("#msg_email_error").hide();
        $("#msg_email_validation_error").hide();
        $("#email_server_error").hide();
    });

    $("#email_conf input").focusin(function () {
        $("#msg_email_conf_error").hide();
    });

    $("#password input").focusin(function () {
        $("#msg_password_error").hide();
        $("#msg_pass_conf_error").hide();
        $("#msg_lowercase_letter").hide();
        $("#msg_capital_letter").hide();
        $("#msg_number").hide();
        $("#msg_length").hide();
    });

    $("#password_conf input").focusin(function () {
        $("#msg_pass_conf_error").hide();
    });
</script>
</html>
