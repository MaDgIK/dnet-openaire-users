<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <base href=".">
    <title>${param.title}</title>
    <script src="js/jquery.js"></script>
    <script src="js/uikit-new.min.js"></script>
    <script src="js/uikit-icons-new.min.js"></script>
    <script src="js/validation.js"></script>
    <link rel="stylesheet" style="text/css" href="css/theme-new.css">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon/favicon-16x16.png">
    <link href="images/favicon/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <c:if test="${param.form == true}">
        <script src='js/input.js'></script>
    </c:if>
</head>