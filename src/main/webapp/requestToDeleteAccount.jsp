<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en-gb" dir="ltr" vocab="http://schema.org/">
<jsp:include page="head.jsp">
  <jsp:param name="title" value="OpenAIRE - Request to Delete Account"/>
  <jsp:param name="form" value="true"/>
</jsp:include>
<body>
<div class="uk-section uk-section-small uk-container uk-container-small">
  <div class="uk-text-center">
    <img src="images/Logo_Horizontal.png" style="height: 80px;">
    <h1 class="uk-h4 uk-margin-large-top">Request to delete your account</h1>
    <div class="uk-margin-large-bottom uk-margin-medium-top">
      Please enter the <span class="uk-text-bolder">email address</span> of your account. A verification code will be
      sent to you. Once you have received
      the verification code, you will be able to delete your account.
    </div>
  </div>
  <form action="requestToDeleteAccount" method="POST" role="form"
        class="uk-grid uk-child-width-1-1 uk-flex-column uk-flex-middle" uk-grid>
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    <div input id="email" class="uk-width-medium@s">
      <div class="input-wrapper inner x-small">
        <div class="input-box">
          <div class="placeholder">
            <label>Email <sup>*</sup></label>
          </div>
          <div class="uk-flex uk-flex-middle">
            <input name="email" class="input uk-text-truncate">
          </div>
        </div>
      </div>
      <span id="msg_email_error" class="uk-text-danger uk-text-small">
        ${message}
      </span>
      <c:remove var="message" scope="session"/>
    </div>
    <div class="uk-width-1-1 uk-text-center">
      <div class="uk-flex uk-flex-center">
        <div class="g-recaptcha" data-sitekey=${applicationScope.sitekey}></div>
      </div>
      <div id="server_error" class="uk-text-danger uk-text-small">
        ${reCAPTCHA_message}
      </div>
      <c:remove var="reCAPTCHA_message" scope="session"/>
    </div>
    <div class="uk-width-1-1 uk-text-center">
      <button type="submit" class="uk-button uk-button-primary" onclick="return validateForm();">
        Submit
      </button>
    </div>
  </form>
</div>
</body>
<script>
    $("#email input").focusin(function () {
        $("#msg_email_error").hide();
        $("#server_error").hide();
    });
</script>
</html>
