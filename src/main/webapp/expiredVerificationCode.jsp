<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<% if (session.getAttribute("expiredVerificationCode") == null) {
    String redirectURL = request.getContextPath() + "/error404.jsp";
    response.sendRedirect(redirectURL);
} else if (session.getAttribute("expiredVerificationCode") != null) {
    session.removeAttribute("expiredVerificationCode");
}%>
<html lang="en-gb" dir="ltr" vocab="http://schema.org/">
<jsp:include page="head.jsp">
    <jsp:param name="title" value="OpenAIRE - Expired Verification Code"/>
</jsp:include>
<body>
<div class="uk-section uk-section-small uk-container uk-container-small">
    <div class="uk-text-center">
        <img src="images/Logo_Horizontal.png" style="height: 80px;">
        <div class="uk-margin-large-top uk-text-warning">
            <span class="material-icons" style="font-size: 180px;">alarm</span>
        </div>
        <div class="uk-text-large uk-text-bold uk-margin-medium-top">Your verification code has expired</div>
        <div class="uk-margin-top">Please request for a new verification code <a href="./requestActivationCode.jsp">here</a>.</div>
    </div>
</div>
</body>
</html>
