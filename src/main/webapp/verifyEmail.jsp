<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en-gb" dir="ltr" vocab="http://schema.org/">
<jsp:include page="head.jsp">
    <jsp:param name="title" value="OpenAIRE - Email Verification"/>
    <jsp:param name="form" value="true"/>
</jsp:include>
<body>
<div class="uk-section uk-section-small uk-container uk-container-small">
    <div class="uk-text-center">
        <img src="images/Logo_Horizontal.png" style="height: 80px;">
        <h1 class="uk-h4 uk-margin-large-top">Email Verification</h1>
        <div class="uk-margin-large-bottom uk-margin-medium-top">
            An <span class="uk-text-bolder">email</span> has been sent to your email address. The email contains a verification code, please paste the verification code in the field below to prove that you are the owner of this account.
        </div>
    </div>
    <form action="verifyCode" method="POST" role="form"
          class="uk-grid uk-child-width-1-1 uk-flex-column uk-flex-middle" uk-grid>
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        <div id="server_error" class="uk-text-danger uk-text-center uk-text-small">${message}</div>
        <c:remove var="message" scope="session" />
        <div input id="username" class="uk-width-medium@s">
            <div class="input-wrapper inner x-small">
                <div class="input-box">
                    <div class="placeholder">
                        <label>Username <sup>*</sup></label>
                    </div>
                    <div class="uk-flex uk-flex-middle">
                        <input name="username" class="input uk-text-truncate">
                    </div>
                </div>
            </div>
            <span id="msg_username_error" class="uk-text-danger uk-text-small" style="display:none">
                Please enter your username.
            </span>
            <c:remove var="msg_username_error" scope="session" />
        </div>
        <div input id="verification_code" class="uk-width-medium@s">
            <div class="input-wrapper inner x-small">
                <div class="input-box">
                    <div class="placeholder">
                        <label>Verification Code <sup>*</sup></label>
                    </div>
                    <div class="uk-flex uk-flex-middle">
                        <input name="verification_code" value="${param.code}" class="input uk-text-truncate">
                    </div>
                </div>
            </div>
            <span id="msg_activation_code_error" class="uk-text-danger uk-text-small" style="display:none">
                Please enter your activation code.
            </span>
        </div>
        <div class="uk-width-1-1 uk-text-center">
            <button type="submit" class="uk-button uk-button-primary" onclick="return validateForm();">
                Submit
            </button>
        </div>
    </form>
</div>
</body>
<script>
    $("#username input").focusin(function() {
        $("#msg_username_error").hide();
        $("#server_error").hide();
    });

    $("#verification_code input").focusin(function() {
        $("#msg_activation_code_error").hide();
        $("#server_error").hide();
    });
</script>
</html>
