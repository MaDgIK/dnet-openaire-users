<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en-gb" dir="ltr" vocab="http://schema.org/">
<jsp:include page="head.jsp">
    <jsp:param name="title" value="OpenAIRE - Request an Activation Code"/>
    <jsp:param name="form" value="true"/>
</jsp:include>
<body>
<div class="uk-section uk-section-small uk-container uk-container-small">
    <div class="uk-text-center">
        <img src="images/Logo_Horizontal.png" style="height: 80px;">
        <h1 class="uk-h4 uk-margin-large-top">Request an activation code</h1>
        <div class="uk-margin-large-bottom uk-margin-medium-top">
            Please enter the <span class="uk-text-bolder">username</span> of your account. A new activation code will be
            sent to you. Once you have received the activation code, you will be able to activate your account.
        </div>
    </div>
    <form action="requestActivationCode" method="POST" role="form"
          class="uk-grid uk-child-width-1-1 uk-flex-column uk-flex-middle" uk-grid>
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        <div input id="username" class="uk-width-medium@s">
            <div class="input-wrapper inner x-small">
                <div class="input-box">
                    <div class="placeholder">
                        <label>Username <sup>*</sup></label>
                    </div>
                    <div class="uk-flex uk-flex-middle">
                        <input name="username" class="input uk-text-truncate">
                    </div>
                </div>
            </div>
            <span id="msg_username_error" class="uk-text-danger uk-text-small">
                ${message}
            </span>
            <c:remove var="message" scope="session"/>
        </div>
        <div class="uk-width-1-1 uk-text-center">
            <div class="uk-flex uk-flex-center">
                <div class="g-recaptcha" data-sitekey=${applicationScope.sitekey}></div>
            </div>
            <div id="server_error" class="uk-text-danger uk-text-small">
                ${reCAPTCHA_message}
            </div>
            <c:remove var="reCAPTCHA_message" scope="session"/>
        </div>
        <div class="uk-width-1-1 uk-text-center">
            <button type="submit" class="uk-button uk-button-primary" onclick="return validateForm();">
                Submit
            </button>
        </div>
    </form>
</div>
</body>
<script>
    $("#username input").focusin(function () {
        $("#msg_username_error").hide();
        $("#server_error").hide();
    });
</script>
</html>
