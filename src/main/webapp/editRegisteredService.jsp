<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en-gb" dir="ltr" vocab="http://schema.org/">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <base href=".">
    <title>OpenAIRE - Register</title>
    <script src="./js/jquery.js"></script>
    <script src="./js/uikit.min.js"></script>
    <script src="./js/validation.js"></script>
    <script src="./js/uikit-icons-max.js"></script>
    <link rel="stylesheet" style="text/css" href="./css/theme.css">
    <link rel="stylesheet" style="text/css" href="./css/custom.css">
    <link rel="stylesheet" style="text/css" href="./css/aai-custom.css">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon//favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon/favicon-16x16.png">
    <link href="images/favicon/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
</head>
<body class="" style="">
<div class="uk-offcanvas-content uk-height-viewport">
    <!-- MENU STARTS HERE -->
    <!-- MAIN MENU STARTS HERE -->
    <div class="tm-header  tm-header-transparent" uk-header="">
        <div class="uk-container uk-container-expand">
            <nav class="uk-navbar" uk-navbar="{&quot;align&quot;:&quot;left&quot;}">
                <div class="uk-navbar-center">
                    <div class="uk-logo uk-navbar-item">
                        <img alt="OpenAIRE" class="uk-responsive-height" src="./images/Logo_Horizontal.png">
                    </div>
                </div>
                <!-- user menu -->
                <div class=uk-navbar-right>
                    <ul class="uk-navbar-nav user_actions">
                        <li><a href="#"> Sign in </a></li>
                        <li>
                            <a class="login uk-icon" aria-expanded="false">
                                <svg height="60" width="60"><circle cx="30" cy="30" r="20" stroke-width="2"></circle>
                                    <text dy=".4em" font-size="16" text-anchor="middle" x="50%" y="50%"> AK </text></svg>
                            </a>
                            <div class="uk-navbar-dropdown uk-navbar-dropdown-bottom-right" id="userMenu" style="left: 344.433px; top: 100px;">
                                <div class="uk-navbar-dropdown-grid uk-child-width-1-1 uk-grid uk-grid-stack" uk-grid="">
                                    <div class="uk-first-column uk-height-max-medium uk-overflow-auto">
                                        <ul class="uk-nav uk-navbar-dropdown-nav">
                                            <li class=""><a href="./personalToken">Personal token</a></li>
                                            <li class=""><a href="./registeredServices">Registered services</a></li>
                                            <li class="uk-nav-divider "></li>
                                            <li>
                                                <a id="logout">Log out</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
    <!-- MENU ENDS HERE -->
    <!-- CONTENT STARTS HERE -->
    <div class="first_page_section uk-section-default uk-section uk-padding-remove-vertical">
        <div class="first_page_banner_headline uk-grid-collapse uk-flex-middle uk-margin-remove-vertical uk-grid" uk-grid="">
        </div>
    </div>
    <div class=" uk-section  uk-margin-small-top uk-container uk-container-large" id="tm-main">
        <div class="uk-grid ">
            <div class="uk-width-1-4@m">
                <div  class="uk-card uk-card-default uk-card-body">
                    <div class="uk-h4">APIs access</div>
                    <ul class="uk-nav uk-nav-default">
                        <li class=""><a href="./personalToken">Personal token</a></li>
                        <li class=""><a href="./registeredServices">Registered services</a></li>
                    </ul>
                </div>
            </div>
            <!-- CENTER SIDE -->
            <div class="uk-width-2-3@l uk-width-2-3@m">

                <h2 class="uk-h2 uk-margin-small-bottom">Edit service</h2>
                <h3 class="uk-h4">Please fill in the updated values for your service</h3>
                            <div class="middle-box text-center loginscreen animated fadeInDown ">
                                <div class="k-width-1-1@m uk-width-1-1@s uk-text-center">
                                    <!-- REGISTER FORM -->
                                    <div id="registerForm">
                                        <form action="editRegisteredService" method="POST" role="form" class="m-t" id="register_form">
                                            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                            <input type="hidden" name="serviceId" value="${serviceId}"/>
                                            <div class="alert alert-success" aria-hidden="true" style="display: none;"></div>
                                            <div class="alert alert-danger" aria-hidden="true" style="display: none;"></div>
                                            <div class="form-group">
                                                <span class="msg_first_name_error uk-text-danger uk-text-small uk-float-left" style='${msg_first_name_error_display}'>Please enter a name for your service.</span>
                                                <input id="first_name" name="first_name" type="text" placeholder="Name (*)" class="form-control" value="${first_name}"></div>
                                                <c:remove var="msg_first_name_error_display" scope="session" />
                                                <c:remove var="first_name" scope="session" />
                                            <div class="form-group">
                                                <textarea id="description" name="description" type="textarea" placeholder="Description:" class="form-control uk-textarea" rows="3" value="${description}">
                                                ${description}</textarea>
                                                <c:remove var="description" scope="session" />
                                                <div class="uk-width-1-1 uk-grid-margin uk-first-column">
                                                    <button type="button" class="uk-button uk-button-default" onclick="document.location.href='./registeredServices'">Cancel</button>
                                                    <button type="submit" class="uk-button uk-button-primary" onclick="return validate();">Update service</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <!-- END OF REGISTER FORM -->
                                    <script>
                                        function validate() {
                                            // Check if name is filled
                                            if($("#first_name").val() != undefined) {
                                                if($.trim($("#first_name").val()).length <= 0) {
                                                    $("#first_name").addClass('uk-input aai-form-danger');
                                                    $(".msg_first_name_error").show();
                                                    return false;
                                                } else {
                                                    $(".msg_first_name_error").hide();
                                                    $("#first_name").removeClass('aai-form-danger');
                                                }
                                                return true;
                                            }
                                        }
                                        $("#first_name").focusin(function () {
                                            $(this).removeClass('aai-form-danger');
                                            $(".msg_first_name_error").fadeOut();
                                        });
                                    </script>
                                </div>
                                </ul>
                            </div>
            </div>
            <!-- END OF CENTER SIDE -->
        </div>
    </div>
    <!-- CONTENT ENDS HERE -->
    <!-- FOOTER STARTS HERE-->
    <div class="custom-footer"  style="z-index: 200;">
          <div class="uk-section-primary uk-section uk-section-small">
            <div class="uk-container">
              <div class="uk-grid-margin uk-grid uk-grid-stack" uk-grid="">
                <div class="uk-width-1-1@m uk-first-column">
                  <div class="uk-margin uk-margin-remove-top uk-margin-remove-bottom uk-text-center">
                    <img alt="OpenAIRE" class="el-image" src="./images/Logo_Horizontal_white_small.png">
                  </div>
                  <div class="footer-license uk-margin uk-margin-remove-bottom uk-text-center uk-text-lead">
                    <div><a href="http://creativecommons.org/licenses/by/4.0/" target="_blank" rel="license"><img alt="Creative" src="./images/80x15.png" style="height: auto; max-width: 100%; vertical-align: middle;"></a>&nbsp;UNLESS OTHERWISE INDICATED, ALL MATERIALS CREATED BY THE OPENAIRE CONSORTIUM ARE LICENSED UNDER A&nbsp;<a href="http://creativecommons.org/licenses/by/4.0/" rel="license">CREATIVE COMMONS ATTRIBUTION 4.0 INTERNATIONAL LICENSE</a>.</div>
                    <div>OPENAIRE IS POWERED BY&nbsp;<a href="http://www.d-net.research-infrastructures.eu/">D-NET</a>.</div>
                  </div>
                  <div class="uk-margin uk-margin-remove-top uk-margin-remove-bottom uk-text-right">
                    <a class="uk-totop uk-icon" href="#" uk-scroll="" uk-totop="">
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
</div>
</body>
</html>
