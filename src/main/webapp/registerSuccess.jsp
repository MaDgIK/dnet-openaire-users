<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<% if (session.getAttribute("registerSuccess") == null) {
    String redirectURL = request.getContextPath() + "/error404.jsp";
    response.sendRedirect(redirectURL);
} else if (session.getAttribute("registerSuccess") != null) {
    session.removeAttribute("registerSuccess");
}%>
<META HTTP-EQUIV=Refresh CONTENT='0.5; URL=<%= session.getAttribute("homeUrl")%>'>
<html lang="en-gb" dir="ltr" vocab="http://schema.org/">
<jsp:include page="head.jsp">
    <jsp:param name="title" value="OpenAIRE - Successful registration"/>
</jsp:include>
<body>
<div class="uk-section uk-section-small uk-container uk-container-small">
    <div class="uk-text-center">
        <img src="images/Logo_Horizontal.png" style="height: 80px;">
        <div class="uk-margin-large-top uk-text-success">
            <span class="material-icons" style="font-size: 180px;">check</span>
        </div>
        <div class="uk-text-large uk-text-bold uk-margin-medium-top">Your account has been successfully activated!</div>
    </div>
</div>
</body>
</html>
