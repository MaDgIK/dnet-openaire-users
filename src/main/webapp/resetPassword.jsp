<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<% if (session.getAttribute("username") == null) {
  String redirectURL = request.getContextPath() + "/forgotPassword.jsp";
  response.sendRedirect(redirectURL);
}%>
<html lang="en-gb" dir="ltr" vocab="http://schema.org/">
<jsp:include page="head.jsp">
  <jsp:param name="title" value="OpenAIRE - Reset password"/>
  <jsp:param name="form" value="true"/>
</jsp:include>
<body>
<div class="uk-section uk-section-small uk-container uk-container-small">
  <div class="uk-text-center">
    <img src="images/Logo_Horizontal.png" style="height: 80px;">
    <h1 class="uk-h4 uk-margin-large-top">Reset password</h1>
    <div class="uk-margin-large-bottom uk-margin-medium-top">
      To complete the password reset process, please enter a new password.
    </div>
  </div>
  <form action="resetPassword" method="POST" role="form"
        class="uk-grid uk-child-width-1-1 uk-flex-column uk-flex-middle" uk-grid>
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    <div input id="password" class="uk-width-medium@s">
      <div class="input-wrapper inner x-small">
        <div class="input-box">
          <div class="placeholder">
            <label>Password <sup>*</sup></label>
          </div>
          <div class="uk-flex uk-flex-middle">
            <input name="password" type="password" class="input uk-text-truncate">
          </div>
        </div>
      </div>
      <div id="msg_pass_error" class="uk-text-danger uk-text-small" style="display: none;">Please enter your password.</div>
      <div id="msg_pass_conf_error" class="uk-text-danger uk-text-small" style="display: none;">The passwords don't match.</div>
      <div id="msg_whitespace" class="uk-text-danger uk-text-small" style="display: none;">No white space allowed.</div>
      <div id="msg_lowercase_letter" class="uk-text-danger uk-text-small" style="display: none;">Please add a lowercase letter.</div>
      <div id="msg_uppercase_letter" class="uk-text-danger uk-text-small" style="display: none;">Please add an uppercase letter.</div>
      <div id="msg_number" class="uk-text-danger uk-text-small" style="display: none;">Please add a number.</div>
      <div id="msg_length" class="uk-text-danger uk-text-small" style="display: none;">Must contains at least 6 characters.</div>
      <div id="msg_invalid_password" class="uk-text-danger uk-text-small" style="display: none;">
        The password must contain a lowercase letter, a capital (uppercase) letter, a number and must be at least 6 characters long. White space character is not allowed.
      </div>
      <c:remove var="msg_pass_conf_error_display" scope="session" />
      <c:remove var="msg_password_error_display" scope="session" />
      <c:remove var="msg_invalid_password_display" scope="session" />
    </div>
    <div input id="password_conf" class="uk-width-medium@s">
      <div class="input-wrapper inner x-small">
        <div class="input-box">
          <div class="placeholder">
            <label>Confirm Password <sup>*</sup></label>
          </div>
          <div class="uk-flex uk-flex-middle">
            <input name="password_conf" type="password" class="input uk-text-truncate">
          </div>
        </div>
      </div>
    </div>
    <div class="uk-width-1-1 uk-flex uk-flex-center">
      <div class="g-recaptcha" data-sitekey=${applicationScope.sitekey}></div>
    </div>
    <div class="uk-width-1-1 uk-text-center">
      <div id="server_error" class="uk-text-danger uk-text-center uk-text-small uk-margin-bottom">${message}</div>
      <c:remove var="message" scope="session" />
      <button type="submit" class="uk-button uk-button-primary" onclick="return validatePasswordForm();">
        Submit
      </button>
    </div>
  </form>
</div>
</body>
<script>
    $("input").focusin(function () {
        $("#server_error").hide();
    });

    // On the fly check for password validation
    let password = $("#password input")[0];
    password.onkeyup = function () {
        validatePassword(password.value);
    };

    $("#password input").focusin(function () {
        $("#msg_password_error").hide();
        $("#msg_pass_conf_error").hide();
        $("#msg_lowercase_letter").hide();
        $("#msg_capital_letter").hide();
        $("#msg_number").hide();
        $("#msg_length").hide();
    });

    $("#password_conf input").focusin(function () {
        $("#msg_pass_conf_error").hide();
    });
</script>
</html>
