function validateForm() {
    let hasError = false;

    // Check if first name is filled
    let firstNameInput = getTextField($("#first_name"));
    if(firstNameInput.val() !== undefined) {
        let wrapper = getWrapper("#first_name");
        if($.trim(firstNameInput.val()).length <= 0) {
            $("#msg_first_name_error").show();
            wrapper.addClass('danger');
            hasError = true;
        } else {
            $("#msg_first_name_error").hide();
            wrapper.removeClass('danger');
        }
    }

    // Check if last name is filled
    let lastNameInput = getTextField($("#last_name"));
    if(lastNameInput.val() !== undefined) {
        let wrapper = getWrapper("#last_name");
        if($.trim(lastNameInput.val()).length <= 0) {
            $("#msg_last_name_error").show();
            wrapper.addClass('danger');
            hasError = true;
        } else {
            $("#msg_last_name_error").hide();
            wrapper.removeClass('danger');
        }
    }

    // Check if username is filled
    let usernameInput = getTextField($("#username"));
    if(usernameInput.val() !== undefined) {
        let wrapper = getWrapper("#username");
        if($.trim(usernameInput.val()).length <= 0) {
            $("#msg_username_error").show();
            wrapper.addClass('danger');
            hasError = true;
        } else {
            $("#msg_username_error").hide();
            wrapper.removeClass('danger');
        }
    }

    // Check if verification code is filled
    let verificationCodeInput = getTextField($("#verification_code"));
    if(verificationCodeInput.val() !== undefined) {
        let wrapper = getWrapper("#verification_code");
        if($.trim(verificationCodeInput.val()).length <= 0) {
            $("#msg_verification_code_error").show();
            $("#msg_activation_code_error").show();
            wrapper.addClass('danger');
            hasError = true;
        } else {
            $("#msg_verification_code_error").hide();
            $("#msg_activation_code_error").hide();
            wrapper.removeClass('danger');
        }
    }

    // Check if email is filled and valid
    let emailInput = getTextField($("#email"));
    let emailConfInput = getTextField($("#email_conf"));
    if(emailInput.val() !== undefined) {
        let wrapper = getWrapper("#email");
        if($.trim(emailInput.val()).length <= 0) {
            wrapper.addClass('danger');
            hasError = true;
            $("#msg_email_error").show();
        } else {
            $("#msg_email_error").hide();
            if(validateEmail(emailInput.val())) {
                $("#msg_email_validation_error").hide();
                if(emailConfInput) {
                    let confWrapper = getWrapper('#email_conf')
                    if(!confirm(emailInput.val(), emailConfInput.val())) {
                        wrapper.addClass('danger');
                        confWrapper.addClass('danger');
                        $("#msg_email_conf_error").show();
                        hasError = true;
                    } else {
                        $("#msg_email_conf_error").hide();
                        wrapper.removeClass('danger');
                        confWrapper.removeClass('danger');
                    }
                }
                wrapper.removeClass('danger');
            } else {
                wrapper.addClass('danger');
                $("#msg_email_validation_error").show();
                hasError = true;
            }
        }
    }

    //Check if password is filled and valid
    let passwordInput = getTextField($("#password"));
    let passwordConfInput = getTextField($("#password_conf"));
    let validate = validatePassword(passwordInput.val(), passwordConfInput.val());
    if(!hasError) {
        hasError = !validate;
    }

    let recaptcha = grecaptcha.getResponse();
    if (recaptcha != null && recaptcha.length > 0) {
        $("#recaptcha_error").hide();

    } else {
        hasError = true;
        $("#recaptcha_error").show();
    }

    return !hasError;
}

function validatePasswordForm() {
    let passwordInput = getTextField($("#password"));
    let passwordConfInput = getTextField($("#password_conf"));
    return validatePassword(passwordInput.val(), passwordConfInput.val());
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function validatePassword(password, confPassword = null) {
    let space = /[\s]+/g;
    let wrapper = getWrapper('#password');
    wrapper.removeClass('danger');
    $("#msg_pass_error").hide();
    $("#msg_whitespace").hide();
    $("#msg_lowercase_letter").hide();
    $("#msg_uppercase_letter").hide();
    $("#msg_number").hide();
    $("#msg_length").hide();
    if(password.length === 0) {
        wrapper.addClass('danger');
        $("#msg_pass_error").show();
        return false;
    } else {
        $("#msg_pass_error").hide();
        if (password.match(space)) {
            wrapper.addClass('danger');
            $("#msg_whitespace").show();
            return false;
        } else {
            $("#msg_whitespace").hide();
            let lowerCaseLetters = /[a-z]/g;
            if (!password.match(lowerCaseLetters)) {
                wrapper.addClass('danger');
                $("#msg_lowercase_letter").show();
                return false;
            } else {
                $("#msg_lowercase_letter").hide();
                let upperCaseLetters = /[A-Z]/g;
                if (!password.match(upperCaseLetters)) {
                    wrapper.addClass('danger');
                    $("#msg_uppercase_letter").show();
                    return false;
                } else {
                    $("#msg_uppercase_letter").hide();
                    let numbers = /[0-9]/g;
                    if (!password.match(numbers)) {
                        wrapper.addClass('danger');
                        $("#msg_number").show();
                        return false;
                    } else {
                        $("#msg_number").hide();
                        if (password.length >= 6) {
                            $("#msg_length").hide();
                            wrapper.removeClass('danger');
                        } else {
                            wrapper.addClass('danger');
                            $("#msg_length").show();
                            return false;
                        }
                    }
                }
            }
    }

        if(confPassword) {
            let confWrapper = getWrapper('#password_conf')
            if(!confirm(password, confPassword)) {
                wrapper.addClass('danger');
                confWrapper.addClass('danger');
                $("#msg_pass_conf_error").show();
                return false;
            } else {
                $("#msg_pass_conf_error").hide();
                wrapper.removeClass('danger');
                confWrapper.removeClass('danger');
            }
        }
        return true;
    }
}

function confirm(first, second) {
    return first === second;
}

function loginForm(){

    // Check if username is filled
    if($.trim($("#login_username").val()).length <= 0) {
        $("#login_username").addClass('uk-input aai-form-danger');
        $(".msg_login_username_error").show();
    } else {
        $(".msg_login_username_error").hide();
        $("#login_username").removeClass('aai-form-danger');
    }

    // Check if password is filled
    if($.trim($("#login_password").val()).length <= 0) {
        $("#login_password").addClass('uk-input aai-form-danger');
        $(".msg_login_password_error").show();
    } else {
        isPasswordFilled = true;
        $(".msg_login_password_error").hide();
        $("#login_password").removeClass('aai-form-danger');
    }
}
