package eu.dnetlib.openaire.usermanagement.utils;

import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by sofia on 8/3/2018.
 */
public class UrlConstructor {

    private static final Logger logger = Logger.getLogger(UrlConstructor.class);

    public static String getRedirectUrl(HttpServletRequest request, String jspPage) {

        String scheme = request.getScheme();
        String serverName = request.getServerName();
        int portNumber = request.getServerPort();
        String contextPath = request.getContextPath();

        String resultPath;
        if (portNumber == 80) {
            resultPath = scheme + "://" + serverName + contextPath + "/" + jspPage;
        } else {
            resultPath = scheme + "://" + serverName + ":" + portNumber + contextPath + "/" + jspPage;
        }

        if (jspPage.equals("error.jsp")) {
            request.getSession().setAttribute("error", "true");
        }
        if (jspPage.equals("success.jsp")) {
            request.getSession().setAttribute("success", "true");
        }
        if (jspPage.equals("successAddPassword.jsp")) {
            request.getSession().setAttribute("successAddPassword", "true");
        }
        if (jspPage.equals("successDeleteAccount.jsp")) {
            request.getSession().setAttribute("successDeleteAccount", "true");
        }
        if (jspPage.equals("expiredVerificationCode.jsp")) {
            request.getSession().setAttribute("expiredVerificationCode", "true");
        }
        if (jspPage.equals("registerSuccess.jsp")) {
            request.getSession().setAttribute("registerSuccess", "true");
        }
        if (jspPage.equals("emailSuccess.jsp")) {
            request.getSession().setAttribute("emailSuccess", "true");
        }

        return resultPath;
    }

    public static String getVerificationLink(String path , String verificationCode ) {

        return path + "?code=" + verificationCode;

    }

}
