package eu.dnetlib.openaire.usermanagement.utils;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import eu.dnetlib.openaire.user.pojos.RoleVerification;
import org.springframework.stereotype.Component;

@Component
public class JsonUtils {

    public JsonObject createVerification(RoleVerification roleVerification) {
        JsonObject verification = new JsonObject();
        verification.addProperty("id", roleVerification.getId());
        verification.addProperty("email", roleVerification.getEmail());
        verification.addProperty("type", roleVerification.getType());
        verification.addProperty("entity", roleVerification.getEntity());
        verification.addProperty("verificationType", roleVerification.getVerificationType());
        verification.addProperty("date", roleVerification.getDate().getTime());
        return verification;
    }

    public JsonObject createResponse(JsonElement response) {
        JsonObject json = new JsonObject();
        json.add("response", response);
        return json;
    }

    public JsonObject createResponse(String response) {
        JsonObject json = new JsonObject();
        json.addProperty("response", response);
        return json;
    }

    public JsonObject createResponse(Number response) {
        JsonObject json = new JsonObject();
        json.addProperty("response", response);
        return json;
    }

    public JsonObject createResponse(Boolean response) {
        JsonObject json = new JsonObject();
        json.addProperty("response", response);
        return json;
    }

    public JsonObject createResponse(Character response) {
        JsonObject json = new JsonObject();
        json.addProperty("response", response);
        return json;
    }
}
