package eu.dnetlib.openaire.usermanagement.utils;

import org.mitre.openid.connect.model.OIDCAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component("AuthorizationService")
public class AuthorizationService {

    public final String PORTAL_ADMIN = "PORTAL_ADMINISTRATOR";
    public final String ANONYMOUS_USER = "ROLE_ANONYMOUS";
    public final String REGISTERED_USER = "REGISTERED_USER";

    private String mapType(String type, boolean communityMap) {
        if(type.equals("organization")) {
            type = "institution";
        } else if(type.equals("ri") && communityMap) {
            type = "community";
        }
        while (type.contains(".")) {
            type = type.replace(".", "_");
        }
        return type;
    }

    /**
     * Type = FUNDER | COMMUNITY | INSTITUTION | PROJECT
     *
     * */
    public String curator(String type) {
        return "CURATOR_" + mapType(type, true).toUpperCase();
    }

    /**
     * Type = FUNDER | COMMUNITY | INSTITUTION | PROJECT
     *
     * Id = EE, EGI, etc
     * */
    public String manager(String type, String id) {
        return mapType(type, true).toUpperCase() + "_" + id.toUpperCase() + "_MANAGER";
    }

    /**
     * Type = FUNDER | COMMUNITY | RI | INSTITUTION | PROJECT
     *
     * Id = EE, EGI, etc
     * */
    public String member(String type, String id) {
        return mapType(type, false).toUpperCase() + "_" + id.toUpperCase();
    }

    public boolean isCommunity(String type) {
        return mapType(type, false).equals("community");
    }

    public boolean isPortalAdmin() {
        return getRoles().stream().anyMatch(authority -> authority.equalsIgnoreCase(PORTAL_ADMIN));
    }

    public boolean isCurator(String type) {
        return getRoles().stream().anyMatch(authority -> authority.equalsIgnoreCase(curator(type)));
    }

    public boolean isManager(String type, String id) {
        return getRoles().stream().anyMatch(authority -> authority.equalsIgnoreCase(manager(type, id)));
    }

    public boolean isMember(String type, String id) {
        return getRoles().stream().anyMatch(authority -> authority.equalsIgnoreCase(member(type, id)));
    }

    public List<String> getRoles() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication instanceof OIDCAuthenticationToken) {
            return authentication.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList());
        }
        return new ArrayList<>();
    }

    public String getAaiId() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication instanceof OIDCAuthenticationToken ? ((OIDCAuthenticationToken)authentication).getSub() : null;
    }

    public String getEmail() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication instanceof OIDCAuthenticationToken ? ((OIDCAuthenticationToken)authentication).getUserInfo().getEmail() : null;
    }
}
