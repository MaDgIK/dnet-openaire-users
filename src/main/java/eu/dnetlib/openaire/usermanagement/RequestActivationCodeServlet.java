package eu.dnetlib.openaire.usermanagement;

import eu.dnetlib.openaire.user.utils.EmailSender;
import eu.dnetlib.openaire.user.utils.LDAPActions;
import eu.dnetlib.openaire.user.utils.VerificationActions;
import eu.dnetlib.openaire.user.utils.VerifyRecaptcha;
import eu.dnetlib.openaire.usermanagement.utils.UrlConstructor;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.mail.MessagingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.UUID;

/**
 * Created by sofia on 14/5/2018.
 */
public class RequestActivationCodeServlet extends HttpServlet {

    @Autowired
    private VerificationActions verificationActions;

    @Autowired
    private LDAPActions ldapActions;

    @Autowired
    private EmailSender emailSender;

    @Value("${oidc.home}")
    private String oidcHomeUrl;

    @Value("${google.recaptcha.secret}")
    private String secret;

    @Value("${google.recaptcha.key}")
    private String sitekey;

    private static final Logger logger = Logger.getLogger(RequestActivationCodeServlet.class);

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this,
                config.getServletContext());
        config.getServletContext().setAttribute("sitekey", sitekey);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String formUsername = request.getParameter("username").trim();

        String gRecaptchaResponse = request.getParameter("g-recaptcha-response");

        HttpSession session = request.getSession();
        session.setAttribute("homeUrl", oidcHomeUrl);

        if (formUsername == null) {
            request.getSession().setAttribute("message", "Error reading username.");
            response.sendRedirect("./requestActivationCode.jsp");

        } else if (formUsername.isEmpty()) {
            request.getSession().setAttribute("message", "Please enter your username.");
            response.sendRedirect("./requestActivationCode.jsp");

        } else if (!VerifyRecaptcha.verify(gRecaptchaResponse, secret)) {
            request.getSession().setAttribute("reCAPTCHA_message", "You missed the reCAPTCHA validation!");
            response.sendRedirect("./requestActivationCode.jsp");

        } else {

            try {
                if (ldapActions.isZombieUsersUsername(formUsername)) {
                    logger.info("User " + formUsername + " is zombie user!");

                    UUID verificationCode = UUID.randomUUID();
                    Date creationDate = new Date();
                    String vCode = verificationCode.toString();

                    Timestamp timestamp = new Timestamp(creationDate.getTime());

                    if (!verificationActions.verificationEntryExists(formUsername)) {
                        verificationActions.addVerificationEntry(formUsername, vCode, timestamp);

                    } else {
                        verificationActions.updateVerificationEntry(formUsername, vCode, timestamp);
                    }

                    String resultPath = UrlConstructor.getRedirectUrl(request, "activate.jsp");
                    String resultPathWithVCode = UrlConstructor.getVerificationLink(resultPath, vCode);

                    String verificationCodeMsg = "<p>Hello " + formUsername + ",</p>" +
                            "<p> A request has been made to get a new activation code to verify your email and activate your OpenAIRE account. To activate your " +
                            "account, you will need to submit your username and this activation code in order to verify that the " +
                            "request was legitimate.</p>" +
                            "<p>" +
                            "The activation code is " + vCode +
                            "</p>" +
                            "Click the URL below and proceed with activating your password." +
                            "<p><a href=" + resultPathWithVCode + ">" + resultPathWithVCode + "</a></p>" +
                            "<p>The activation code is valid for 24 hours.</p>" +
                            "<p>Thank you,</p>" +
                            "<p>OpenAIRE technical team</p>";

                    String verificationCodeSubject = "Request a new activation code for your OpenAIRE account";

                    String email = ldapActions.getZombieUsersEmail(formUsername);

                    if (email != null && !email.isEmpty()) {
                        emailSender.sendEmail(email, verificationCodeSubject, verificationCodeMsg);
                        logger.info("Sending activation code to user: " + formUsername);
                    }

                    response.sendRedirect("./activate.jsp");

                } else if (ldapActions.usernameExists(formUsername)) {
                    logger.info("User " + formUsername + " has already activated his account.");
                    request.getSession().setAttribute("message", "Your account is already activated.");
                    response.sendRedirect("./requestActivationCode.jsp");

                } else {
                    logger.info("No user with username: "  + formUsername);
                    request.getSession().setAttribute("message", "There is no user registered with that username.");
                    response.sendRedirect("./requestActivationCode.jsp");
                }

            } catch (MessagingException e) {
                logger.error("Error in sending email", e);
                request.getSession().setAttribute("message", "Error sending email");
                response.sendRedirect("./requestActivationCode.jsp");
            } catch (Exception ldape) {
                logger.error("Could not find zombie user with username " + formUsername, ldape);
                response.sendRedirect(UrlConstructor.getRedirectUrl(request, "error.jsp"));
            }
        }
    }

    public String getOidcHomeUrl() {
        return oidcHomeUrl;
    }

    public void setOidcHomeUrl(String oidcHomeUrl) {
        this.oidcHomeUrl = oidcHomeUrl;
    }





}
