package eu.dnetlib.openaire.usermanagement;

import eu.dnetlib.openaire.user.utils.EmailSender;
import eu.dnetlib.openaire.user.utils.LDAPActions;
import eu.dnetlib.openaire.user.utils.VerificationActions;
import eu.dnetlib.openaire.user.utils.VerifyRecaptcha;
import eu.dnetlib.openaire.usermanagement.utils.UrlConstructor;
import org.apache.commons.validator.routines.EmailValidator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.mail.MessagingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.UUID;

/**
 * Created by sofia on 21/5/2018.
 */
public class RequestToDeleteAccountServlet extends HttpServlet {

    @Autowired
    private VerificationActions verificationActions;

    @Autowired
    private LDAPActions ldapActions;

    @Autowired
    private EmailSender emailSender;

    @Value("${oidc.home}")
    private String oidcHomeUrl;

    @Value("${google.recaptcha.secret}")
    private String secret;

    @Value("${google.recaptcha.key}")
    private String sitekey;

    private static final Logger logger = Logger.getLogger(RequestActivationCodeServlet.class);

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this,
                config.getServletContext());
        config.getServletContext().setAttribute("sitekey", sitekey);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String formEmail = request.getParameter("email").trim();

        String gRecaptchaResponse = request.getParameter("g-recaptcha-response");

        HttpSession session = request.getSession();
        session.setAttribute("homeUrl", oidcHomeUrl);

        if (formEmail == null) {
            request.getSession().setAttribute("message", "Error reading email.");
            response.sendRedirect("./requestToDeleteAccount.jsp");

        } else if (formEmail.isEmpty()) {
            request.getSession().setAttribute("message", "Please enter your email.");
            response.sendRedirect("./requestToDeleteAccount.jsp");

        } else if (!EmailValidator.getInstance().isValid(formEmail)) {
            request.getSession().setAttribute("message", "Please enter a valid email.");
            response.sendRedirect("./requestToDeleteAccount.jsp");

        } else if (!VerifyRecaptcha.verify(gRecaptchaResponse, secret)) {
            request.getSession().setAttribute("reCAPTCHA_message", "You missed the reCAPTCHA validation!");
            response.sendRedirect("./requestToDeleteAccount.jsp");

        } else {

            try {

                Boolean isRegistered = false;
                Boolean isZombie = false;

                if (ldapActions.emailExists(formEmail)) {
                    logger.info("User with email: " + formEmail + " is activated user!");
                    isRegistered = true;
                } else if (ldapActions.isZombieUsersEmail(formEmail)) {
                    logger.info("User with email: " + formEmail + " is zombie user!");
                    isZombie = true;
                }

                if (!isRegistered && !isZombie) {
                    request.getSession().setAttribute("message", "There is no user with that email.");
                    response.sendRedirect("./requestToDeleteAccount.jsp");
                } else {

                    String username = null;

                    if (isRegistered) {
                        username = ldapActions.getUsername(formEmail);
                    } else if (isZombie) {
                        username = ldapActions.getZombieUsersUserName(formEmail);
                    }

                    UUID verificationCode = UUID.randomUUID();
                    Date creationDate = new Date();
                    String vCode = verificationCode.toString();

                    Timestamp timestamp = new Timestamp(creationDate.getTime());

                    if (!verificationActions.verificationEntryExists(username)) {
                        verificationActions.addVerificationEntry(username, vCode, timestamp);

                    } else {
                        verificationActions.updateVerificationEntry(username, vCode, timestamp);
                    }

                    String resultPath = UrlConstructor.getRedirectUrl(request, "verifyToDelete.jsp");
                    String resultPathWithVCode = UrlConstructor.getVerificationLink(resultPath, vCode);

                    String verificationCodeMsg = "<p>Hello " + username + ",</p>" +
                            "<p> A request has been made to get a verification code to delete your OpenAIRE account. To delete your " +
                            "account, you will need to submit your username and this verification code in order to verify that the " +
                            "request was legitimate.</p>" +
                            "<p>" +
                            "The verification code is " + vCode +
                            "</p>" +
                            "Click the URL below and proceed with deleting your account." +
                            "<p><a href=" + resultPathWithVCode + ">" + resultPathWithVCode + "</a></p>" +
                            "<p>The verification code is valid for 24 hours.</p>" +
                            "<p>Thank you,</p>" +
                            "<p>OpenAIRE technical team</p>";

                    String verificationCodeSubject = "Request to delete your OpenAIRE account";

                    emailSender.sendEmail(formEmail, verificationCodeSubject, verificationCodeMsg);
                    logger.info("Sending verification code to user: " + formEmail);


                    response.sendRedirect("./verifyToDelete.jsp");
                }
            } catch (MessagingException e) {
                logger.error("Error in sending email", e);
                request.getSession().setAttribute("message", "Error sending email");
                response.sendRedirect("./requestActivationCode.jsp");
            } catch (Exception ldape) {
                logger.error("Could not user with email " + formEmail, ldape);
                response.sendRedirect(UrlConstructor.getRedirectUrl(request, "error.jsp"));
            }
        }
    }

    public String getOidcHomeUrl() {
        return oidcHomeUrl;
    }

    public void setOidcHomeUrl(String oidcHomeUrl) {
        this.oidcHomeUrl = oidcHomeUrl;
    }
}
