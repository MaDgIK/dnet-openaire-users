package eu.dnetlib.openaire.usermanagement;

import eu.dnetlib.openaire.user.utils.InputValidator;
import eu.dnetlib.openaire.user.utils.VerificationActions;
import eu.dnetlib.openaire.usermanagement.utils.UrlConstructor;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by kiatrop on 28/9/2017.
 */
public class VerificationCodeServlet extends HttpServlet {

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this,
                config.getServletContext());
    }

    @Autowired
    private VerificationActions verificationActions;

    private Logger logger = Logger.getLogger(VerificationCodeServlet.class);

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        PrintWriter printWriter = response.getWriter();

        String formUsername = request.getParameter("username").trim();
        String formVerificationCode = request.getParameter("verification_code").trim();

        if (InputValidator.isFilled(formUsername) && InputValidator.isFilled(formVerificationCode)) {
            if (verificationActions.verificationEntryExists(formUsername) && verificationActions.verificationCodeIsCorrect(formUsername, formVerificationCode)) {
                if (!verificationActions.verificationCodeHasExpired(formUsername)) {
                    HttpSession session = request.getSession();
                    session.setAttribute("username", formUsername);
                    response.sendRedirect("./resetPassword.jsp");
                } else {
                    logger.info("Verification code has expired!");
                    response.sendRedirect(UrlConstructor.getRedirectUrl(request, "expiredVerificationCode.jsp"));
                }
            } else {
                logger.info("Username or verification code are not valid!");
                request.getSession().setAttribute("message", "Username or verification code are not valid.");
                response.sendRedirect("./verify.jsp");
            }
        } else {
            if (!InputValidator.isFilled(formUsername)) {
                logger.info("No username");
                request.getSession().setAttribute("msg_username_error", "Please enter your username.");
            }
            if (!InputValidator.isFilled(formVerificationCode)) {
                logger.info("No verification code");
                request.getSession().setAttribute("msg_verification_code_error", "Please enter your verification code.");
            }
            response.sendRedirect("./verify.jsp");
            }

        printWriter.close();
    }
}
