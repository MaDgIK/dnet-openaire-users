package eu.dnetlib.openaire.usermanagement.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Role {
    String name;
    String description;

    public Role() {}

    public Role(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
