package eu.dnetlib.openaire.usermanagement;

import eu.dnetlib.openaire.user.utils.InputValidator;
import eu.dnetlib.openaire.user.utils.LDAPActions;
import eu.dnetlib.openaire.user.utils.VerificationActions;
import eu.dnetlib.openaire.usermanagement.utils.UrlConstructor;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by sofia on 23/10/2017.
 */
public class AddPasswordServlet extends HttpServlet {

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this,
                config.getServletContext());
    }

    @Autowired
    private VerificationActions verificationActions;

    @Autowired
    private LDAPActions ldapActions;

    private Logger logger = Logger.getLogger(AddPasswordServlet.class);

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        PrintWriter printWriter = response.getWriter();

        HttpSession session = request.getSession();
        String username = (String) session.getAttribute("username");

        if (username == null){
            logger.info("Empty username in session");
        }

        String password = request.getParameter("password");
        String confirmPassword = request.getParameter("password_conf");

        if (InputValidator.isFilled(password)) {
            if (InputValidator.isValidPassword(password) && password.equals(confirmPassword) && username !=null) {
                try {
                    ldapActions.resetPassword(username, password);
                    logger.info("password added");
                    session.removeAttribute("username");
                    response.sendRedirect(UrlConstructor.getRedirectUrl(request, "success.jsp"));
                } catch (Exception e) {
                    logger.error("LDAP error in adding password", e);
                    response.sendRedirect(UrlConstructor.getRedirectUrl(request, "error.jsp"));
                }
            } else {
                if (!InputValidator.isValidPassword(password)) {
                    logger.info("No valid password");
//                    request.getSession().setAttribute("msg_invalid_password", "The password must contain a lowercase letter, a capital (uppercase) letter, a number and must be at least 6 characters long. White space character is not allowed.");
                }
                if (!password.equals(confirmPassword)) {
                    logger.info("No matching passwords");
//                    request.getSession().setAttribute("msg_pass_conf_error", "These passwords don't match.");
                }
                response.sendRedirect("./addPassword.jsp");
            }
        } else {
            logger.info("Empty password");
            request.getSession().setAttribute("msg_password_error_display", "display:block" );
//            request.getSession().setAttribute("msg_invalid_password", "The password must contain a lowercase letter, a capital (uppercase) letter, a number and must be at least 6 characters long. White space character is not allowed.");
            response.sendRedirect("./resetPassword.jsp");
        }

        printWriter.close();

    }
}
