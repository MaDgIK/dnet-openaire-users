package eu.dnetlib.openaire.usermanagement.authorization;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import org.apache.log4j.Logger;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collection;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AuthoritiesMapper {

    private static final Logger logger = Logger.getLogger(AuthoritiesMapper.class);

    public static Collection<? extends GrantedAuthority> map(JsonArray entitlements) {
        HashSet<SimpleGrantedAuthority> authorities = new HashSet<>();
        String regex = "urn:geant:openaire[.]eu:group:([^:]*):?(.*)?:role=member#aai[.]openaire[.]eu";
        for(JsonElement obj: entitlements) {
            Matcher matcher = Pattern.compile(regex).matcher(obj.getAsString());
            if (matcher.find()) {
                StringBuilder sb = new StringBuilder();
                if(matcher.group(1) != null && matcher.group(1).length() > 0) {
                    sb.append(matcher.group(1).replace("+-+", "_").replaceAll("[+.]", "_").toUpperCase());
                }
                if(matcher.group(2).length() > 0) {
                    sb.append("_");
                    if(matcher.group(2).equals("admins")) {
                        sb.append("MANAGER");
                    } else  {
                        sb.append(matcher.group(2).toUpperCase());
                    }
                }
                authorities.add(new SimpleGrantedAuthority(sb.toString()));
            }
        }
        return authorities;
    }
}
